#!/bin/sh

set -eu

. common/update-test-path

export LD_LIBRARY_PATH=${ARCHDIR}/bin:${ARCHDIR}/lib/x86_64-linux-gnu/canterbury
export PATH=${ARCHDIR}/bin/:$PATH

FAILURE_DETECTED=0
TESTONLY=""

while [ $# -gt 0 ] ; do
	case $1 in
	"-x") set -x ;;
	"-t") TESTONLY=$2; shift ;;
	esac
	shift
done

unmountdir() {
	sudo sh -c "umount $1 || true"
	sudo rmdir $1  || true
}

unmount () {
	unmountdir /usr/share/applications > /dev/null 2>&1
	unmountdir /usr/Applications > /dev/null 2>&1
}

pass() {
	echo "$(basename $1) ... ok"
}

fail() {
	echo "$(basename $1) ... FAIL"
	FAILURE_DETECTED=1
}

skip() {
	echo "$(basename $1) ... SKIP"
}

trap unmount EXIT
sudo mount --bind ${ARCHDIR}/Applications /usr/Applications
sudo mount --bind ${ARCHDIR}/share/applications /usr/share/applications

TESTS=""
while read -r SECTION CONTENT ; do
	if [ "${SECTION}" = "Tests:" ] ; then
		for WORD in ${CONTENT} ; do
			TESTS="${TESTS} ${WORD}"
		done
	fi
done < ${ARCHDIR}/lib/installed-tests/canterbury-0/control

for TEST in ${TESTS} ; do

	if [ -n "${TESTONLY}" ] && [ "${TESTONLY}" != $(basename "${TEST}") ] ; then
		continue
	fi

	case "${TEST}" in
	"agents-slow"|"build"|"core"|"core-as-root"|"full"|"full-as-root"\
	|"store-app-bundle-apparmor")
		continue
	esac

	case "${TEST}" in
	"agent-after-reboot"\
	|"agent-after-reboot-full")
		# Those are manual tests
		continue
	esac

	ASROOT=''
	USERNAME=user
	case "${TEST}" in
	"core-as-root"\
	|"full-as-root")
		ASROOT=sudo
		USERNAME=root
	esac

	mkdir -p /tmp/cby
	TEMPDIR=`mktemp -d -p /tmp/cby`
	cp ./${ARCHDIR}/lib/installed-tests/canterbury-0/${TEST} ${TEMPDIR}
	TEST=${TEMPDIR}/${TEST}

	sed "s|/bin/bash|/bin/sh|" -i ${TEST}
	sed "s|-o pipefail||" -i ${TEST}
	sed "s|/usr/|$(pwd)/${ARCHDIR}/|" -i ${TEST}

	if $ASROOT ${TEST}; then
		pass ${TEST}
		continue
	fi

	fail ${TEST}
done

mkdir -p /tmp/cbyrw/
TESTS="${ARCHDIR}/share/installed-tests/canterbury-0/*"
for TEST in ${TESTS} ; do

	if [ -n "${TESTONLY}" ] && [ "${TESTONLY}" != $(basename "${TEST}") ] ; then
		skip "${TEST}"
		continue
	fi

	if [ -z "${TESTONLY}" ] ; then
		case $(basename "${TEST}") in
		"agents.t.test"|"dconf.t.test"|"request-manager.t.test"|"terminate.t.test"\
		|"process-info.t.test"|"activation.t.test"|"launch-preferences.t.test")
			skip "${TEST}"
			continue
		esac
	fi

	# Skip those until ubercache is enabled again T6243
	if [ -z "${TESTONLY}" ] ; then
		case $(basename "${TEST}") in
		"postinst-prerm-core.t.test"|"postinst-prerm.t.test")
			skip "${TEST}"
			continue
		esac
	fi

	# Installed tests contains a hardcoded /usr, replace it by absolute path,
	# Do that in a temp directory because lava root fs is readonly
	TEMPTESTDIR=`mktemp -d -p /tmp/cbyrw`
	FULLTESTDIR=${TEMPTESTDIR}/installed-tests/canterbury-0
	mkdir -p ${FULLTESTDIR}/
	cp ./${TEST} ${FULLTESTDIR}/
	TEST=$(basename ${TEST})
	sed "s|/usr/|$(pwd)/${ARCHDIR}/|" -i ${FULLTESTDIR}/${TEST}

	# Some tests install glib schemas, fake that by bind mounting new schemas
	if [ "$TEST" = "launch-preferences.t.test" ] ||
		[ "$TEST" = "dconf.t.test" ] ; then
		mkdir -p /tmp/cbyschemas
		TEMPDIR=`mktemp -d -p /tmp/cbyschemas`
		sudo rm -f ${TEMPDIR}/*
		sudo cp /usr/share/glib-2.0/schemas/* ${TEMPDIR}/
		sudo cp ${ARCHDIR}/share/glib-2.0/schemas/org.apertis.Canterbury.Config.gschema.xml ${TEMPDIR}/
		sudo mount --bind ${TEMPDIR}/ /usr/share/glib-2.0/schemas/
		sudo glib-compile-schemas /usr/share/glib-2.0/schemas/
	fi

	# Some tests run as root
	ASROOT=''
	USERNAME=user
	case "${TEST}" in
	"terminate.t.test"\
	|"postinst-prerm-core.t.test"\
	|"postinst-prerm.t.test"\
	|"service-manager.t.test")
		ASROOT=sudo
		USERNAME=root
	esac

	# Run the tests
	if $ASROOT sh -c "G_MESSAGES_DEBUG=all LD_LIBRARY_PATH=$(pwd)/${ARCHDIR}/bin:$(pwd)/${ARCHDIR}/lib ${ARCHDIR}/bin/gnome-desktop-testing-runner -d ${TEMPTESTDIR}/ canterbury-0/${TEST}"; then
		pass ${TEST}
		continue
	fi

	if [ "$TEST" = "launch-preferences.t.test" ] ||
		[ "$TEST" = "dconf.t.test" ] ; then
		sudo umount /usr/share/glib-2.0/schemas/
	fi

	fail ${TEST}
done

exit $FAILURE_DETECTED
